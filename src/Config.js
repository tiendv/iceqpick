import device;

var CLOUD_SCALE = 0.7;
var BOX_SCALE = 0.8;
var TILE_SCALE = 0.8;
var BUTTON_SCALE = device.width / 576;
var BG_WIDTH = 576;
var BG_HEIGHT = 1024;
var PLAYER_SCALE = 0.5;
var PLAYER_OFFSETX = (BG_WIDTH - 540 * PLAYER_SCALE) / 2;
var TILE_WIDTH = 226 * TILE_SCALE;
var TILE_HEIGHT = 154 * TILE_SCALE;
var TILE_VELOCITY = 0.8;
var TILE_OFFSETX = (BG_WIDTH - 226 * TILE_SCALE) / 2 + 40 * TILE_SCALE;
var TILE_HIT_BOUNDS_MARGIN_X = 10;
var TILE_HIT_BOUNDS_MARGIN_Y = 15;
var TILE_HIT_BOUNDS_WIDTH = 166 * TILE_SCALE;
var BOX_WIDTH = 312 * BOX_SCALE;
var BOX_HEIGHT = 174 * BOX_SCALE;
var BOX_OFFSETY = BG_HEIGHT - BOX_HEIGHT - 0.5 * BOX_HEIGHT;
var FALLING_OFFSETY = BOX_OFFSETY + BOX_HEIGHT - 90 * BOX_SCALE;   // -85 because the box image have a margin at bottom.
var PLAYER_OFFSETY = BG_HEIGHT / 2 - 616 * PLAYER_SCALE + 0.75 * TILE_HEIGHT;
var TILE_OFFSETY = PLAYER_OFFSETY - 154 * TILE_SCALE / 2 + 616 * PLAYER_SCALE + 20;  // Should be a relative number to Player offsetY
var TILE_RELATIVEY = - 154 * TILE_SCALE / 2 + 616 * PLAYER_SCALE + 20;
var BUTTON_WIDTH = 115;
var BUTTON_HEIGHT = 115;

exports = {
  maxTick: 100,
  bgWidth: BG_WIDTH,
  bgHeight: BG_HEIGHT,
  player_offsetX: PLAYER_OFFSETX,
  tile_offsetX: TILE_OFFSETX,
  tile_offsetY: TILE_OFFSETY,
  tile_ralativeY: TILE_RELATIVEY,
  tile_velocity: TILE_VELOCITY,
  tile_width: TILE_WIDTH,
  tile_height: TILE_HEIGHT,
  falling_offsetY: FALLING_OFFSETY,
  gameOverDelay: 3000,
  backgroundLayers: [
    {
      id: 'bg',
      spawnCount: 3,
      spawnBounded: true,
      xMultiplier: 0,
      xCanSpawn: false,
      xCanRelease: false,
      yMultiplier: 0.15,
      yCanSpawn: true,
      yCanRelease: false,
      ordered: true,
      pieceOptions: [
        { image: "resources/images/game/bg_1.png" },
        { image: "resources/images/game/bg_3.png" },
        { image: "resources/images/game/bg_2.png" }
      ]
    },
    {
      id: 'clouds',
      spawnCount: 10,
      xMultiplier: 0,
      xCanSpawn: true,
      xCanRelease: false,
      yMultiplier: 0.17,
      yCanSpawn: true,
      yCanRelease: false,
      yGapRange: [300, BG_HEIGHT],
      yLimitMax: BG_HEIGHT / 16,
      pieceOptions: [
        {
          xAlign: 'center',
          width: 232 * CLOUD_SCALE,
          height: 140 * CLOUD_SCALE,
          image: "resources/images/game/cloud_02.png"
        },
        {
          xAlign: 'center',
          width: 450 * CLOUD_SCALE,
          height: 210 * CLOUD_SCALE,
          image: "resources/images/game/cloud_03.png"
        }
      ]
    },
    {
      id: 'clouds2',
      spawnCount: 8,
      xMultiplier: -0.1,
      xCanSpawn: true,
      xCanRelease: false,
      yMultiplier: 0.17,
      yCanSpawn: true,
      yCanRelease: false,
      yGapRange: [1000, BG_HEIGHT * 3],
      yLimitMax: BG_HEIGHT / 16,
      pieceOptions: [
        {
          styleRanges: { x: [BG_WIDTH, BG_WIDTH * 3] },
          xAlign: 'center',
          width: 450 * CLOUD_SCALE,
          height: 220 * CLOUD_SCALE,
          image: "resources/images/game/cloud_01.png"
        }
      ]
    },
    {
      id: 'clouds3',
      spawnCount: 8,
      xMultiplier: 0.1,
      xCanSpawn: true,
      xCanRelease: false,
      yMultiplier: 0.17,
      yCanSpawn: true,
      yCanRelease: false,
      yGapRange: [1000, BG_HEIGHT * 3],
      yLimitMax: BG_HEIGHT / 16,
      pieceOptions: [
        {
          styleRanges: { x: [-BG_WIDTH, BG_WIDTH/2] },
          xAlign: 'center',
          width: 450 * CLOUD_SCALE,
          height: 220 * CLOUD_SCALE,
          image: "resources/images/game/cloud_01.png"
        }
      ]
    },
    {
      id: 'wires',
      spawnCount: 3,
      xMultiplier: 0,
      xCanSpawn: false,
      xCanRelease: false,
      yMultiplier: 0.4,
      yCanSpawn: true,
      yCanRelease: false,
      yGapRange: [100, 500],
      yLimitMax: BG_HEIGHT / 16,
      pieceOptions: [
        {
          x: 0,
          y: -300,
          opacity: 0.7,
          zIndex: 10,
          width: 576,
          height: 220 * CLOUD_SCALE,
          image: "resources/images/game/wire_01.png"
        },
        {
          x: 0, 
          y: -BG_HEIGHT + 100,
          zIndex: 10,
          width: 576,
          height: 140 * CLOUD_SCALE,
          image: "resources/images/game/wire_02.png"
        },
        {
          x: 0,
          y: -2 * BG_HEIGHT + 450,
          zIndex: 10,
          width: 576,
          height: 210 * CLOUD_SCALE,
          image: "resources/images/game/wire_03.png"
        }
      ]
    },
    {
      id: 'stars',
      spawnCount: 5, 
      xMultiplier: 0,
      xCanSpawn: false,
      xCanRelease: false,
      yMultiplier: 0.2, 
      yCanSpawn: true,
      yCanRelease: false,
      yGapRange: [200, 500],
      pieceOptions: [
        {
          styleRanges: { x: [0, BG_WIDTH], y: [-BG_HEIGHT, 0] },
          xAlign: 'center',
          width: 98,
          height: 89,
          image: "resources/images/game/star_01.png"
        },
        {
          styleRanges: { x: [0, BG_WIDTH], y: [- 2 * BG_HEIGHT + 100, -BG_HEIGHT + 200] },
          xAlign: 'center',
          width: 98,
          height: 89,
          image: "resources/images/game/star_02.png"
        }        
      ]
    },
    {
      id: 'particle_stars',
      spawnCount: 30, 
      xMultiplier: 0,
      xCanSpawn: false,
      xCanRelease: false,
      yMultiplier: 0.13, 
      yCanSpawn: true,
      yCanRelease: false,
      yGapRange: [100, 300],
      pieceOptions: [
        {
          styleRanges: { x: [0, BG_WIDTH]},
          xAlign: 'center',
          width: 134,
          height: 134,
          image: "resources/images/game/particle_stars.png"
        }     
      ]
    },
    {
      id: 'cityBack',
      spawnCount: 1,
      xMultiplier: 0,
      xCanSpawn: false,
      xCanRelease: false,
      yMultiplier: 0.18,
      yCanSpawn: true,
      yCanRelease: false,
      pieceOptions: [
        {
          y: BG_HEIGHT,
          yAlign: 'bottom',
          image: "resources/images/game/city_back.png"
        }
      ]
    },
    {
      id: 'cityMiddle',
      spawnCount: 1,
      xMultiplier: 0,
      xCanSpawn: false,
      xCanRelease: false,
      yMultiplier: 0.25,
      yCanSpawn: true,
      yCanRelease: false,
      pieceOptions: [
        {
          y: BG_HEIGHT,
          yAlign: 'bottom',
          image: "resources/images/game/city_middle.png"
        }
      ]
    },
    {
      id: 'cityFront',
      spawnCount: 1,
      xMultiplier: 0,
      xCanSpawn: false,
      xCanRelease: false,
      yMultiplier: 0.33,
      yCanSpawn: true,
      yCanRelease: false,
      pieceOptions: [
        {
          y: BG_HEIGHT,
          yAlign: 'bottom',
          image: "resources/images/game/city_front.png"
        }
      ]
    },
    {
      id: 'foreground',
      spawnCount: 1,
      xMultiplier: 0,
      xCanSpawn: false,
      xCanRelease: false,
      yMultiplier: 0.4,
      yCanSpawn: true,
      yCanRelease: false,
      pieceOptions: [
        {
          y: BG_HEIGHT,
          width: BG_WIDTH,
          height: BG_HEIGHT,
          yAlign: 'bottom',
          image: "resources/images/game/foreground.png"
        }
      ]
    }
  ],
  playerEntityPool: {
    tile_height: TILE_HEIGHT - 2 * TILE_HIT_BOUNDS_MARGIN_Y,
    offsetX: PLAYER_OFFSETX,
    offsetY: PLAYER_OFFSETY,
    dotted_box: {
      x: TILE_OFFSETX + 2,
      y: TILE_OFFSETY,
    hitOpts: {
      width: 172,
      height: 142
    },
    viewOpts: {
      width: 172,
      height: 142,
      opacity: 0.7,
      url: "resources/images/game/dotted_box.png"
    }          
    },
    box: {
      x: (BG_WIDTH - BOX_WIDTH) / 2 + 20,
      y: BOX_OFFSETY,
    hitOpts: {
      width: BOX_WIDTH,
      height: BOX_HEIGHT,
    },
    viewOpts: {
      zIndex: 500,
      width: BOX_WIDTH,
      height: BOX_HEIGHT,
      url: "resources/images/game/box.png"
    }          
    },
	  koala: {
	    x: PLAYER_OFFSETX,
	    y: PLAYER_OFFSETY,
		hitOpts: {
		  width: 540 * PLAYER_SCALE,
		  height: 616 * PLAYER_SCALE
		},
		viewOpts: {
		  zIndex: 500,
		  width: 540 * PLAYER_SCALE,
		  height: 616 * PLAYER_SCALE,
		  defaultAnimation: "idle",
		  autoStart: true,
		  loop: true,
		  url: "resources/images/game/koala/koala"
		}
	  },
	  blimp: {
	    x: PLAYER_OFFSETX,
	    y: PLAYER_OFFSETY,
		hitOpts: {
		  width: 540 * PLAYER_SCALE,
		  height: 616 * PLAYER_SCALE
		},
		viewOpts: {
		  zIndex: 0,
		  width: 540 * PLAYER_SCALE,
		  height: 616 * PLAYER_SCALE,
		  url: "resources/images/game/koala/blimp.png"
		}	
	  },
	  dotted_line: {
	    x: 0,
	    y: FALLING_OFFSETY,

		hitOpts: {
		  width: 576,
		  height: 21
		},
		viewOpts: {
		  zIndex: 0,
		  width: 576,
		  height: 21,
		  url: "resources/images/game/dotted_line.png"
		}	
	  }	  
  },
  tileObjects: {
  	types: [
  		{
  			id: "tile_cat_orange",
			x: TILE_OFFSETX,
			y: TILE_OFFSETY,
  			hitOpts: {
          offsetX: TILE_HIT_BOUNDS_MARGIN_X,
          offsetY: TILE_HIT_BOUNDS_MARGIN_Y,
  				width: TILE_HIT_BOUNDS_WIDTH,
				height: TILE_HEIGHT - TILE_HIT_BOUNDS_MARGIN_Y
  			},
			viewOpts: {
			  width: TILE_WIDTH,
			  height: TILE_HEIGHT,
        r: 0,
			  url: "resources/images/game/tile_cat_orange.png"
			}	
  		},
      {
        id: "tile_cat_red",
      x: TILE_OFFSETX,
      y: TILE_OFFSETY,
        hitOpts: {
          offsetX: TILE_HIT_BOUNDS_MARGIN_X,
          offsetY: TILE_HIT_BOUNDS_MARGIN_Y,
          width: TILE_HIT_BOUNDS_WIDTH,
        height: TILE_HEIGHT - TILE_HIT_BOUNDS_MARGIN_Y
        },
      viewOpts: {
        width: TILE_WIDTH,
        height: TILE_HEIGHT,
        r: 0,
        url: "resources/images/game/tile_cat_red.png"
      } 
      },
      {
        id: "tile_mouse",
      x: TILE_OFFSETX,
      y: TILE_OFFSETY,
        hitOpts: {
          offsetX: TILE_HIT_BOUNDS_MARGIN_X,
          offsetY: TILE_HIT_BOUNDS_MARGIN_Y,
          width: TILE_HIT_BOUNDS_WIDTH,
        height: TILE_HEIGHT - TILE_HIT_BOUNDS_MARGIN_Y
        },
      viewOpts: {
        width: TILE_WIDTH,
        height: TILE_HEIGHT,
        r: 0,
        url: "resources/images/game/tile_mouse.png"
      }
      },
      {
        id: "tile_racoon",
      x: TILE_OFFSETX,
      y: TILE_OFFSETY,
        hitOpts: {
          offsetX: TILE_HIT_BOUNDS_MARGIN_X,
          offsetY: TILE_HIT_BOUNDS_MARGIN_Y,
          width: TILE_HIT_BOUNDS_WIDTH,
        height: TILE_HEIGHT - TILE_HIT_BOUNDS_MARGIN_Y
        },
      viewOpts: {
        width: TILE_WIDTH,
        height: TILE_HEIGHT,
        r: 0,
        url: "resources/images/game/tile_racoon.png"
      } 
      },
      {
        id: "tile_star_blue",
        x: TILE_OFFSETX,
        y: TILE_OFFSETY,
        hitOpts: {
          offsetX: TILE_HIT_BOUNDS_MARGIN_X,
          offsetY: TILE_HIT_BOUNDS_MARGIN_Y,
          width: TILE_HIT_BOUNDS_WIDTH,
        height: TILE_HEIGHT - TILE_HIT_BOUNDS_MARGIN_Y
        },
      viewOpts: {
        width: TILE_WIDTH,
        height: TILE_HEIGHT,
        r: 0,
        url: "resources/images/game/tile_star_blue.png"
      }
      },
      {
        id: "tile_star_green",
        x: TILE_OFFSETX,
        y: TILE_OFFSETY,
        hitOpts: {
          offsetX: TILE_HIT_BOUNDS_MARGIN_X,
          offsetY: TILE_HIT_BOUNDS_MARGIN_Y,
          width: TILE_HIT_BOUNDS_WIDTH,
        height: TILE_HEIGHT - TILE_HIT_BOUNDS_MARGIN_Y
        },
      viewOpts: {
        width: TILE_WIDTH,
        height: TILE_HEIGHT,
        r: 0,
        url: "resources/images/game/tile_star_green.png"
      }
      },
      {
        id: "tile_star_purple",
        x: TILE_OFFSETX,
        y: TILE_OFFSETY,
        hitOpts: {
          offsetX: TILE_HIT_BOUNDS_MARGIN_X,
          offsetY: TILE_HIT_BOUNDS_MARGIN_Y,
          width: TILE_HIT_BOUNDS_WIDTH,
        height: TILE_HEIGHT - TILE_HIT_BOUNDS_MARGIN_Y
        },
      viewOpts: {
        width: TILE_WIDTH,
        height: TILE_HEIGHT,
        r: 0,
        url: "resources/images/game/tile_star_purple.png"
      }
      },
      {
        id: "tile_star_yellow",
        x: TILE_OFFSETX,
        y: TILE_OFFSETY,
        hitOpts: {
          offsetX: TILE_HIT_BOUNDS_MARGIN_X,
          offsetY: TILE_HIT_BOUNDS_MARGIN_Y,
          width: TILE_HIT_BOUNDS_WIDTH,
        height: TILE_HEIGHT - TILE_HIT_BOUNDS_MARGIN_Y
        },
      viewOpts: {
        width: TILE_WIDTH,
        height: TILE_HEIGHT,
        r: 0,
       url: "resources/images/game/tile_star_yellow.png"
      }
      },       
  	]
  },
  scoreView: {
    x: 0,
    y: 0,
    width: 576,
    height: 100,
    text: "0000",
    horizontalAlign: "center",
    spacing: -24,
    characterData: {
      "0": {image: "resources/images/game/txt_0.png"},
      "1": {image: "resources/images/game/txt_1.png"},
      "2": {image: "resources/images/game/txt_2.png"},
      "3": {image: "resources/images/game/txt_3.png"},
      "4": {image: "resources/images/game/txt_4.png"},
      "5": {image: "resources/images/game/txt_5.png"},
      "6": {image: "resources/images/game/txt_6.png"},
      "7": {image: "resources/images/game/txt_7.png"},
      "8": {image: "resources/images/game/txt_8.png"},
      "9": {image: "resources/images/game/txt_9.png"}
    }
  }, 
  heightView: {
    x: 8,
    y: FALLING_OFFSETY - 40,  
    width: 576,
    height: 50,
    text: "00",
    horizontalAlign: "left",
    spacing: -8,
    characterData: {
      "0": {image: "resources/images/game/txt_0.png"},
      "1": {image: "resources/images/game/txt_1.png"},
      "2": {image: "resources/images/game/txt_2.png"},
      "3": {image: "resources/images/game/txt_3.png"},
      "4": {image: "resources/images/game/txt_4.png"},
      "5": {image: "resources/images/game/txt_5.png"},
      "6": {image: "resources/images/game/txt_6.png"},
      "7": {image: "resources/images/game/txt_7.png"},
      "8": {image: "resources/images/game/txt_8.png"},
      "9": {image: "resources/images/game/txt_9.png"}
    }
  },
  pause_button: {
      x: 0,
      y: BG_HEIGHT - BUTTON_HEIGHT,
      zIndex: 1000,
      width: BUTTON_WIDTH,
      height: BUTTON_HEIGHT,
      image: "resources/images/game/button_pause.png"    
  },
  timerView: {
    x: BG_WIDTH - BUTTON_WIDTH,
    y: (BG_HEIGHT - 105),
    width: 466,
    height: 95,
    zIndex: 1000,
    timer_full: {
      image: "resources/images/game/timer_big_full.png",
      x: 0,
      y: 0,
      width: 466,
      height: 95,
      zIndex: 999,
    },
    timer_empty: {
      image: "resources/images/game/timer_big_empty.png",
      x: 110,
      y: 0,
      width: 466,
      height: 95,
      zIndex: 998
    },
    number: {
    width: 576,
    height: 50,
    text: "00",
    horizontalAlign: "left",
    spacing: -8,
    characterData: {
      "0": {image: "resources/images/game/timer_numbers/0.png"},
      "1": {image: "resources/images/game/timer_numbers/1.png"},
      "2": {image: "resources/images/game/timer_numbers/2.png"},
      "3": {image: "resources/images/game/timer_numbers/3.png"},
      "4": {image: "resources/images/game/timer_numbers/4.png"},
      "5": {image: "resources/images/game/timer_numbers/5.png"},
      "6": {image: "resources/images/game/timer_numbers/6.png"},
      "7": {image: "resources/images/game/timer_numbers/7.png"},
      "8": {image: "resources/images/game/timer_numbers/8.png"},
      "9": {image: "resources/images/game/timer_numbers/9.png"}
    }
    }
  }
};
